module UsersHelper

	def gravatar_for user 
		gid = Digest::MD5::hexdigest(user.email.downcase)
		gurl = "https://secure.gravatar.com/avatar/#{gid}"
		image_tag(gurl, alt: user.name, class: "gravatar")		
	end

end
