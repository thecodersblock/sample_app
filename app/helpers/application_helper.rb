module ApplicationHelper

	def setTitle title
		if title.empty?
			"Untitled  | Ruby on Rails Tutorial Sample App"
		else
			title+" | Ruby on Rails Tutorial Sample App"
		end
	end

end
